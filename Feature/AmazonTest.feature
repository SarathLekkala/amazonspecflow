﻿Feature: AmazonTest
	To validate Amazon online store functionalities

Background:
	Given the user launches amazon url
	When the user Maximise the broswer window 

Scenario: To Verify Amazon site is loaded successfully
	
	Then the user check for Amazon logo on homespage
	And close the browser instance

Scenario: To Verify Amazon AddtoCart screen is loaded successfully
	 
	Given the user Clicks on addtocart logo
	Then  the user check for Your Amazon Basket is empty on cart page
	Then  close the browser instance

Scenario: To Verify Search by category Books
	
	Given the user select Books from category dropdown
	Then  the user perform search icon click
	Then  close the browser instance

Scenario Outline:: To verify user as ability to Search by product name and to check it avaiablility
	
	Given the user search with product name "<productName>"
	Then  the user check the product "<productName>" avaiability in cart
	Then  close the browser instance
	 Examples:
    | productName                                              |
    | Apple iPhone 11 Pro (256GB) - Silver                     |
    | Samsung Galaxy M31 (Space Black, 8GB RAM, 128GB Storage) |

Scenario Outline:: To verify user as ability to Search by product name and add to cart
	
	Given the user search with product name "<productName>"
	Then  the user navigate to product screen "<productName>"
	Then  the user add the product "<productName>" to cart
	Then  close the browser instance
	 Examples:
    | productName                                              |
    | Apple iPhone 11 Pro (256GB) - Silver                     |
    
   