﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Chrome;

namespace AmazonSpecflow.BaseClass
{
   
    public  class BaseTest
    {
        public static IWebDriver driver;
        public string homeURL;
        public void LaunchUrl()
        {
            homeURL = "https://www.amazon.in/";
            driver = new ChromeDriver(@"C:\Users\RAJESHVARRA\source\repos\AmazonSpecflow\Drivers\");
            driver.Navigate().GoToUrl(homeURL);

        }
        public void MaximiseWindow()
        {
            driver.Manage().Window.Maximize();
        }
        public void CloseBrowser()
        {
            driver.Quit();
        }
    }
}
