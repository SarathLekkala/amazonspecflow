﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmazonSpecflow.PageObjects
{
  public  class AddToCartPage
    {
        IWebDriver driver;
        public AddToCartPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);

        }
            

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'sc-your-amazon-cart-is-empty')]")]
        public IWebElement CartEmptyelement { get; set; }

        
        public string ValideEmptyCart()
        {
            return CartEmptyelement.Text;
        }
       
    }
}
