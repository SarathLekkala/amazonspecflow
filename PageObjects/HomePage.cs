﻿using System;
using System.Collections.Generic;
using System.Text;
using AmazonSpecflow.BaseClass;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;

namespace AmazonSpecflow.PageObjects
{
    public class HomePage
    {

        IWebDriver driver;
        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            ;
        }

        [FindsBy(How=How.XPath,Using= "//*[@id='nav-logo-sprites']")]
        public IWebElement amazonLogo { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='nav-cart-count-container']/span[2]")]
        public IWebElement addToCarteElement { get; set; }

        public string LoadHomescreen()
        {
            string amazonlogoText;
            WebDriverWait wait = new WebDriverWait(driver, System.TimeSpan.FromSeconds(15));
            wait.Until(driver => amazonLogo);
            amazonlogoText = amazonLogo.GetAttribute("aria-label");
            return amazonlogoText;
        }
        public void NavigateToCart()
        {
            addToCarteElement.Click();


        }
               
        
    }
}
