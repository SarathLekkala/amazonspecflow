﻿using AmazonSpecflow.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using AmazonSpecflow.BaseClass;
using NUnit.Framework;

namespace AmazonSpecflow.StepDefination
{
    [Binding]
    public sealed class HomePageStep : BaseTest
    {


        private readonly ScenarioContext _scenarioContext;

        public HomePageStep(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Then(@"the user check for Amazon logo on homespage")]
        public void ThenTheUserCheckForAmazonLogoOnHomespage()
        {
            HomePage hp = new HomePage(driver);

            Assert.AreEqual("Amazon", hp.LoadHomescreen());
        }

        [Given(@"the user Clicks on addtocart logo")]
        public void WhenTheUserClicksOnAddtocartLogo()
        {
            HomePage hp = new HomePage(driver);
            hp.NavigateToCart();


        }
    }
}
