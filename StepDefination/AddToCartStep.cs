﻿using AmazonSpecflow.BaseClass;
using AmazonSpecflow.PageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace AmazonSpecflow.StepDefination
{
    [Binding]
    public sealed class AddToCartStep: BaseTest
    {
       

        private readonly ScenarioContext _scenarioContext;

        public AddToCartStep(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        //Step defination to validate blank cart screen is loaded oncliking cart icon
        [Then(@"the user check for Your Amazon Basket is empty on cart page")]
        public void ThenTheUserCheckForYourAmazonBasketIsEmptyOnCartPage()
        {
            AddToCartPage emptycart = new AddToCartPage(driver);
            string emptycartpage = emptycart.ValideEmptyCart();
            Assert.AreEqual("Your Amazon Basket is empty", emptycartpage);
        }

    }
}
