﻿using AmazonSpecflow.BaseClass;
using AmazonSpecflow.PageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace AmazonSpecflow.StepDefination
{
    [Binding]
    public sealed class SearchPageStep:BaseTest
    {
        

        private readonly ScenarioContext _scenarioContext;

        public SearchPageStep(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        // To select books from the category dropdown on home screen
        [Given(@"the user select Books from category dropdown")]
        public void WhenTheUserSelectBooksFromCategoryDropdown()
        {
            SearchPage sp = new SearchPage(driver);
            sp.searchByCategory("Books");

        }

        //Step to perform click on search icon on home screen
        [Then(@"the user perform search icon click")]
        public void ThenTheUserPerformSearchIconClick()
        {
            ResultPage resultSscreen = new ResultPage(driver);
            Assert.AreEqual("Books", resultSscreen.IsSearchByCategory());
        }

        //Step to input product name on search textbox
        [Given(@"the user search with product name ""(.*)""")]
        public void WhenTheUserSearchWithProductName(string productName)
        {
            SearchPage sp = new SearchPage(driver);

            sp.searchWithProductName(productName);

        }
    }
}
