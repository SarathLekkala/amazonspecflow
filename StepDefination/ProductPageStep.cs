﻿using AmazonSpecflow.BaseClass;
using AmazonSpecflow.PageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace AmazonSpecflow.StepDefination
{
    [Binding]
    public sealed class ProductPageStep:BaseTest
    {
        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;

        public ProductPageStep(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        //Step to Navigate to individual product screen

        [Then(@"the user navigate to product screen ""(.*)""")]
        public void ThenTheUserNavigateToProductScreen(string productName)
        {
            ProductPage product = new ProductPage(driver);
            string productsearched = product.NavigateToProductscreen(productName);
        }

        //Step to add the product to cart and verify the cart page is loaded
        [Then(@"the user add the product ""(.*)"" to cart")]
        public void ThenTheUserAddTheProductToCart(string productName)
        {
            ProductPage product = new ProductPage(driver);
            string Addedsuccess = product.AddToCart(productName);
            Assert.AreEqual("Added to Cart", Addedsuccess);

        }

    }
}
