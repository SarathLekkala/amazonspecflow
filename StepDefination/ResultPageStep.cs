﻿using AmazonSpecflow.BaseClass;
using AmazonSpecflow.PageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace AmazonSpecflow.StepDefination
{
    [Binding]
    public sealed class ResultPageStep:BaseTest
    {
        

        private readonly ScenarioContext _scenarioContext;

        public ResultPageStep(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        // Step defination to search with product name and verify the product is avaiable in result page
        [Then(@"the user check the product ""(.*)"" avaiability in cart")]
        public void ThenTheUserCheckTheProductAvaiabilityInCart(string productName)
        {
            ResultPage resultSscreen = new ResultPage(driver);

            Assert.AreEqual("Avaiable", resultSscreen.IsProductAvaialble(productName));
        }
    }
}
