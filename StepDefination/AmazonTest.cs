﻿using AmazonSpecflow.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using AmazonSpecflow.BaseClass;
using NUnit.Framework;

namespace AmazonSpecflow.StepDefination
{
    [Binding]
    public class AmazonTestSteps:BaseTest
    {
      
    
       private readonly ScenarioContext context;

        public AmazonTestSteps(ScenarioContext injectedContext)
        {
            context = injectedContext;
        }

        // step defination to launch Amazon URL in Chrome driver
        [Given(@"the user launches amazon url")]
        public void GivenTheUserLaunchesAmazonUrl()
        {
            LaunchUrl();
        }

        //step defination to maximise Chrome browser
        [When(@"the user Maximise the broswer window")]
        public void WhenTheUserMaximiseTheBroswerWindow()
        {
            MaximiseWindow();
        }

        //step defination to close Chrome browser
        [Then(@"close the browser instance")]
        public void ThenCloseTheBrowserInstance()
        {
            CloseBrowser();
        }
        
         
     


    }
}
